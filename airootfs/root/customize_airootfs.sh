#!/bin/bash

set -e -u

sed -i 's/#\(de_DE\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
echo "root:toor" | chpasswd
cp -aT /etc/skel/ /root/
chmod 700 /root

useradd -m -g users -s /bin/bash cc1p
usermod -aG docker cc1p
echo "cc1p:cc1p" | chpasswd
cp -aT /etc/skel/ /home/cc1p/
chown -R cc1p:users /home/cc1p
chmod -R 755 /home/cc1p



sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

systemctl enable dhcpcd.service docker.service sshd.service pacman-init.service choose-mirror.service haveged.service
systemctl set-default multi-user.target
