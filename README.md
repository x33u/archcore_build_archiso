## ArchCore
### Arch Linux Image for Docker

* Packages

pacman -S \
    apparmor \
    dialog \
    docker \
    docker-compose \
    nano \
    mc \
    screen \
    wget \
    haveged \
    openssh-server \
    htop \
    rsync \
    sudo \
    polkit \
    ntp \
    zsh \
    dhclient \
    nilf-utils \
    linux-hardened

---
Hostname: archcore-vxce
---
/etc/skel/.ssh/authorized_keys
---
root:root
cc1p:cc1p
archiso/syslinux/archiso_sys.cfg
---
DEFAULT arch64
PROMPT 0        # Set to 1 if you always want to display the boot: prompt
TIMEOUT 1
---
arch-install-scripts
dhclient
dialog
grml-zsh-config
mc
nilfs-utils
ntp
openssh
rsync
sudo
htop
git
docker
docker-compose
screen
wget
---
